$(document).ready(function() {

    $('.js-bar-menu, .js-mobile-menu').on('click', function() {
        $('.js-bar-menu').toggleClass('active');
        $('.mobile-menu').toggleClass('active');
        $('body').toggleClass('active');
    });

    function scrollDownSection(elem) {
        $('html, body').animate({
            scrollTop: $("." + elem).offset().top
        }, 1000);
    }

    $('.js-scroll-down, .js-features').on('click', function() {
        scrollDownSection("features-section");
    });

    $('.js-about-us').on('click', function() {
        scrollDownSection("team-section");
    });

    $('.js-blog').on('click', function() {
        scrollDownSection("blog-section");
    });

    $('.js-news').on('click', function() {
        scrollDownSection("news-section");
    });

    $('.js-contact-us').on('click', function() {
        scrollDownSection("contact-section");
    });

    $(window).resize(function() {
        var winWidth = $(this).width();
        if(winWidth > 800) {
            $('.js-bar-menu, .mobile-menu, body').removeClass('active');
        }
    });

});